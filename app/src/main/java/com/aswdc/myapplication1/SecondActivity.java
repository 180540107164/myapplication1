package com.aswdc.myapplication1;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
public class SecondActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);


        Intent intent = getIntent();
        String st = getIntent().getExtras().getString("value");

        TextView textView = (TextView)findViewById(R.id.text);
        textView.setText(st);

//        Toast.makeText(getApplicationContext(),"", Toast.LENGTH_SHORT).show();

    }
}
